forward='''
r0=r0^0x55
r1=r1^(((r7&r2)|r3)+r5)
r4=r4*0x53
r2=~r2
r3=r3-r1
r7=-r7
r5=r5+(r6<<5|r7>>3)
if r4 <= 0x4b:
	r6=r6^(r4*0x41)
r0, r1, r2, r3, r4, r5, r6, r7 = r1, r2, r3, r4, r5, r6, r7, r0+0x4c
'''

def div8(r, d):
	for i in range(256):
		if (i*d)&0xff == r:
			return i
	raise Expection('Invalid div')

def onestep(r):
	r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7] = r[7] - 0x04c, r[0], r[1], r[2], r[3], r[4], r[5], r[6]
	if (r[4] <= 0x4b):
		r[6] = (r[6]^((r[4]*0x41)&0xff))&0xff
	r[5]=(256+r[5]-(((r[6]<<5)&0xff)|((r[7]>>3)&0xff)))&0xff
	r[7]=256-r[7]
	r[3]=(r[3]+r[1])&0xff
	r[2]=(~r[2])&0xff
	r[4]=div8(r[4],0x53)
	r[1]=r[1]^((((r[7]&r[2])|r[3])+r[5])&0xff)
	r[0]=(r[0]^0x55)&0xff
	return r

r=[0xc8,0xb6,0x0f,0xec,0x6c,0x94,0xad,0x5d]
for i in range(0x45):
	r=onestep(r)
for i in range(8):
	print('r[{}] = {};'.format(i, r[i]))
print(''.join([chr(x) for x in r]))
